import numpy as np
import matplotlib.pyplot as plt

data = np.fromfile('data/gl-latlong-1km-landcover.bsq', dtype='uint8')

# Image Size:43200 Pixels 21600 Lines
data = data.reshape(21600, 43200)
# print(data)
#plt.imshow(data[::50, ::50])
# plt.show()


def get_pixel_index(WE, NS):
    WE_pixel_index = 21599 + (WE/0.0083333333333333)
    NS_pixel_index = 10799 + (NS/0.0083333333333333)
    return int(WE_pixel_index), int(NS_pixel_index)


plt.imshow(data[::50, ::50])
# plt.show()
